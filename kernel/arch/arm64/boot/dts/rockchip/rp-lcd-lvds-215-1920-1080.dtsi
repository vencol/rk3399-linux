/*
 * Copyright (c) 2017 Fuzhou Rockchip Electronics Co., Ltd
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

/ {
	backlight: backlight {
		status = "okay";
		compatible = "pwm-backlight";
		pwms = <&pwm0 0 25000 0>;
		brightness-levels = <
			  0   1   2   3   4   5   6   7
			  8   9  10  11  12  13  14  15
			 16  17  18  19  20  21  22  23
			 24  25  26  27  28  29  30  31
			 32  33  34  35  36  37  38  39
			 40  41  42  43  44  45  46  47
			 48  49  50  51  52  53  54  55
			 56  57  58  59  60  61  62  63
			 64  65  66  67  68  69  70  71
			 72  73  74  75  76  77  78  79
			 80  81  82  83  84  85  86  87
			 88  89  90  91  92  93  94  95
			 96  97  98  99 100 101 102 103
			104 105 106 107 108 109 110 111
			112 113 114 115 116 117 118 119
			120 121 122 123 124 125 126 127
			128 129 130 131 132 133 134 135
			136 137 138 139 140 141 142 143
			144 145 146 147 148 149 150 151
			152 153 154 155 156 157 158 159
			160 161 162 163 164 165 166 167
			168 169 170 171 172 173 174 175
			176 177 178 179 180 181 182 183
			184 185 186 187 188 189 190 191
			192 193 194 195 196 197 198 199
			200 201 202 203 204 205 206 207
			208 209 210 211 212 213 214 215
			216 217 218 219 220 221 222 223
			224 225 226 227 228 229 230 231
			232 233 234 235 236 237 238 239
			240 241 242 243 244 245 246 247
			248 249 250 251 252 253 254 255>;
		default-brightness-level = <255>;
	};

        rpdzkj_config {
                compatible = "rp_config";
                user_version = "rpdzkj";
                system_rotate = "0";
                back_camera_rotate = "0";
                front_camera_rotate = "0";
                lcd_density = "160";//320
                language = "zh-CN";   //zh-CN //en-US
                time_zone = "Asia/Shanghai"; //Asia/Shanghai = +8   //Europe/London  = +0
                not_navigation_bar = "false";
                not_status_bar = "false";
                //not_status_bar = "true";
                default_launcher = "true";
                //default_launcher = "false";
                has_root = "true";  //true
                //has_root = "false";  //false
                usb_not_permission = "true";  
                usb_camera_only_front = "false";
                //usb_camera_only_front = "true";
                gps_use = "false";
                gps_serial_port = "/dev/ttyS4"; //UART4
                status = "okay";
        };

};

&hdmi {
       #address-cells = <1>;
       #size-cells = <0>;
       #sound-dai-cells = <0>;
       ddc-i2c-scl-high-time-ns = <9625>;
       ddc-i2c-scl-low-time-ns = <10000>;
       status = "okay";
};


&pwm0 {
	status = "okay";
};

&display_subsystem {
        status = "okay";

        ports = <&vopb_out>, <&vopl_out>;
        logo-memory-region = <&drm_logo>;

        route {
                route_hdmi: route-hdmi {
                        status = "okay";
                        logo,uboot = "logo.bmp";
                        logo,kernel = "logo_kernel.bmp";
                        logo,mode = "fullscreen";
                        charge_logo,mode = "center";
                        connect = <&vopl_out_hdmi>;
                };

                route_dsi: route-dsi {

                        status = "okay";
                        //status = "disabled";
                        logo,uboot = "logo.bmp";
                        logo,kernel = "logo_kernel.bmp";
                        logo,mode = "fullscreen";
                        charge_logo,mode = "center";
                        connect = <&vopb_out_dsi>;
                };

                route_dsi1: route-dsi1 {
                        status = "disabled";
                        logo,uboot = "logo.bmp";
                        logo,kernel = "logo_kernel.bmp";
                        logo,mode = "fullscreen";
                        charge_logo,mode = "center";
                        connect = <&vopl_out_dsi1>;
                };

                route_edp: route-edp {
                        status = "disabled";
                        logo,uboot = "logo.bmp";
                        logo,kernel = "logo_kernel.bmp";
                        logo,mode = "fullscreen";
                        charge_logo,mode = "center";
                        connect = <&vopb_out_edp>;
                };
        };
};

&dsi {
       compatible = "rockchip,rk3399-dsi";
       reset-gpios = <&gpio1 0 GPIO_ACTIVE_LOW>;
       status = "okay";

       panel {
               compatible = "simple-panel-dsi";
               status = "okay";
               reg = <0>;
               power-supply = <&vcc3v3_sys>;
               backlight = <&backlight>;

		dsi,flags = <(MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_BURST |
			      MIPI_DSI_MODE_LPM | MIPI_DSI_MODE_EOT_PACKET)>;
		dsi,format = <MIPI_DSI_FMT_RGB888>;
		dsi,lanes = <4>;

		panel-init-sequence = [
			29 00 06 14 01 08 00 00 00
			29 00 06 3c 01 0c 00 0a 00
			29 00 06 64 01 0c 00 00 00
			29 00 06 68 01 0c 00 00 00
			29 00 06 6c 01 0c 00 00 00
			29 00 06 70 01 0c 00 00 00
			29 00 06 34 01 1f 00 00 00
			29 00 06 10 02 1f 00 00 00
			29 00 06 04 01 01 00 00 00
			29 00 06 04 02 01 00 00 00
			29 00 06 50 04 00 01 f0 03
			29 00 06 54 04 14 00 64 00
			29 00 06 58 04 80 07 a0 00
			29 00 06 5c 04 0a 00 19 00
			29 00 06 60 04 38 04 0a 00
			29 00 06 64 04 01 00 00 00
			29 00 06 a0 04 06 c0 00 00
			29 00 06 04 05 04 00 00 00
			29 00 06 80 04 00 01 02 03
			29 00 06 84 04 04 07 05 08
			29 00 06 88 04 09 0a 0e 0f
			29 00 06 8c 04 0b 0c 0d 10
			29 00 06 90 04 16 17 11 12
			29 00 06 94 04 13 14 15 1b
			29 14 06 98 04 18 19 1a 06
			29 78 06 9c 04 33 04 00 00
		];

		display-timings {
			native-mode = <&timing0>;

			timing0: timing0 {
				clock-frequency = <148500000>;
				hactive = <1920>;
				vactive = <1080>;
				hback-porch = <100>;
				hsync-len = <20>;
				hfront-porch = <160>;
				vback-porch = <25>;
				vfront-porch = <10>;
				vsync-len = <10>;
				hsync-active = <0>;
				vsync-active = <0>;
				de-active = <0>;
				pixelclk-active = <0>;
			};
		};
	};
};

&route_dsi {
        status = "okay";
};

&vopb {
        assigned-clocks = <&cru DCLK_VOP0_DIV>;
        assigned-clock-parents = <&cru PLL_CPLL>;
        //assigned-clock-parents = <&cru PLL_VPLL>;
        status = "okay";
};

&vopb_mmu {
        status = "okay";
};

&vopl {
        assigned-clocks = <&cru DCLK_VOP1_DIV>;
        assigned-clock-parents = <&cru PLL_VPLL>;
        //assigned-clock-parents = <&cru PLL_CPLL>;
        status = "okay";
};

&vopl_mmu {
        status = "okay";
};

&dsi_in_vopb {
        status = "disabled";
};

&hdmi_in_vopl {
        status = "disabled";
};

&edp_in_vopb {
        status = "disabled";
};
&edp_in_vopl {
        status = "disabled";
};

&dsi1_in_vopl {
        status = "disabled";
};
&dsi1_in_vopb {
        status = "disabled";
};
